"__author__ = 'Leo Chan'"
"__credits__ = 'Keboola 2018'"
"__project__ = 'kbc_bluefox'"

"""
Python 3 environment 
"""

import sys
import os
import logging
import csv
import json
import pandas as pd
import requests
import logging_gelf.formatters
import logging_gelf.handlers
from keboola import docker


### Environment setup
abspath = os.path.abspath(__file__)
script_path = os.path.dirname(abspath)
os.chdir(script_path)

### Logging
logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s - %(levelname)s - %(message)s',
    datefmt="%Y-%m-%d %H:%M:%S")
"""
logger = logging.getLogger()
logging_gelf_handler = logging_gelf.handlers.GELFTCPSocketHandler(
    host=os.getenv('KBC_LOGGER_ADDR'),
    port=int(os.getenv('KBC_LOGGER_PORT'))
    )
logging_gelf_handler.setFormatter(logging_gelf.formatters.GELFFormatter(null_character=True))
logger.addHandler(logging_gelf_handler)

# removes the initial stdout logging
logger.removeHandler(logger.handlers[0])
"""

### Access the supplied rules
cfg = docker.Config('/data/')
params = cfg.get_parameters()
baseUrl = cfg.get_parameters()["baseUrl"]
access_token = cfg.get_parameters()["#access_token"]
secret_token = cfg.get_parameters()["#secret_token"]

### Get proper list of tables
cfg = docker.Config('/data/')
in_tables = cfg.get_input_tables()
out_tables = cfg.get_expected_output_tables()
logging.info("IN tables mapped: "+str(in_tables))
logging.info("OUT tables mapped: "+str(out_tables))

### destination to fetch and output files
DEFAULT_FILE_INPUT = "/data/in/tables/"
DEFAULT_FILE_DESTINATION = "/data/out/tables/"


def get_tables(in_tables):
    """
    Evaluate input and output table names.
    Only taking the first one into consideration!
    """

    ### input file
    table = in_tables[0]
    in_name = table["full_path"]
    in_destination = table["destination"]
    logging.info("Data table: " + str(in_name))
    logging.info("Input table source: " + str(in_destination))
    
    return in_name

def get_output_tables(out_tables):
    """
    Evaluate output table names.
    Only taking the first one into consideration!
    """

    ### input file
    table = out_tables[0]
    in_name = table["full_path"]
    in_destination = table["source"]
    logging.info("Data table: " + str(in_name))
    logging.info("Input table source: " + str(in_destination))

    return in_name

def post_request(access_token_in, secret_token_in, endpoint):
    """
    Handling all POST requests
    """

    ### Request Parameters
    request_url = baseUrl+endpoint
    headers = {
        "Accept": "application/plain",
        "Content-Type": "application/plain",
        "x-api-access-token": access_token_in,
        "x-api-secret-token": secret_token_in
    }
    body = {}
    logging.info("Request: {0}, URL: {1}".format(endpoint, request_url))

    data_in = requests.post(request_url, data=json.dumps(body), headers=headers)
    logging.info("Request Status: {0}".format(data_in.status_code))

    return  data_in.json()

def get_account_info():
    """
    Getting respective account information
    """

    endpoint = "get_account_info"
    data_in = post_request(access_token, secret_token, endpoint)

    return data_in

def get_location_info(data):
    """
    Getting information on each location from the array
    """

    data_out = []
    endpoint = "get_location_info"
    for location in data:
        ### Request parameters
        logging.info("Processing Location: {0} - {1}".format(location["nickname"], location["location_uuid"]))
        access_token_temp = location["access_token"]
        secret_token_temp = location["secret_token"]
        data_in = post_request(access_token_temp, secret_token_temp, endpoint)

        ### Extract location info
        location_info = data_in["location_info"]
        data_out.append(location_info)
    
    return data_out

def main():
    """
    Main execution script.
    """

    ### Requests
    account_info = get_account_info() ### Details of the account
    locations = account_info["account_info"]["locations"] ### List of locations within the account
    location_info = get_location_info(locations)

    ### CSV Parsing
    data_out = pd.DataFrame(location_info)
    data_out.to_csv(DEFAULT_FILE_DESTINATION+"location_info.csv", index=False)


    return


if __name__ == "__main__":

    main()

    logging.info("Done.")
